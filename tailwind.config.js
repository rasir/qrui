/** @type {import('tailwindcss').Config} */
export default {
  content: ['./src/**/*.{js,ts,jsx,tsx,mdx}'],
  theme: {
    extend: {
      backgroundColor: {
        'qrui-primary-base-color':'#35f'
      }
    },
  },
  plugins: [],
};
