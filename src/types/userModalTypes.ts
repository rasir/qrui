import type { SelectOption, keyValue } from './index';
import type { ModeEnum } from './enums';

export type BdpSchConfigs = Partial<{
  SCH_TYPE_DEV: boolean;
  SCH_OPTIONS_CREATE_TASK_TYPES: SelectOption[];
  SCH_BUTTON_TASK_EDIT: boolean;
  SCH_BUTTON_DBTB_RULE: boolean;
  SCH_BUTTON_QUALITY: boolean;
  SCH_BUTTON_QUALITY_V3: boolean; // 第三版质量拦截
  SCH_BUTTON_DEV_TASK_DETAIL: boolean;
  SCH_INPUT_SPEED_UP: boolean;
  SCH_TIP_DEV_NEW_TASK_LEVEL: string;
  SCH_TIP_DEV_OLD_TASK_LEVEL: string;
  SCH_TIP_DEV_DEVELOPER: string;
  SCH_TIP_DEV_DEVOPSER: string;
  SCH_BUTTON_SCH_CONFIG: boolean;
  SCH_LINK_RELY_RELATION: boolean;
  SCH_BUTTON_RECENT_USED_RESOURCE: boolean;
  SCH_INPUT_ADD_RELY_TASK_BLOOD_CHECKBOX: boolean;
  SCH_BUTTON_ALARM_CONFIG: boolean;
  SCH_OPTIONS_ALARM_NOTICE_TYPE: SelectOption[];
  SCH_LINK_ETL_DATA_FILTER: string;
  SCH_OPTIONS_ETL_SRC_DATASOURCES: SelectOption[];
  SCH_OPTIONS_ETL_DEST_DATASOURCES: SelectOption[];
  SCH_TIP_ETL_LETTER_INIT: string;
  SCH_LINK_ETL_SPLIT_KEYS: boolean;
  SCH_COL_ETL_IS_ENCRYPT: boolean;
  SCH_COL_ETL_ENCRYPT_WAY: boolean;
  SCH_OPTIONS_ETL_ENCRPT_WAY: SelectOption[]; // ETL字段加密方式
  SCH_OPTIONS_CREATE_FLOW_NODE_TYPES: SelectOption[];
  SCH_BUTTON_FLOW_NODE_RECENT_USED_RESOURCE: boolean;
  SCH_TYPE_DEVOPS: boolean;
  SCH_OPTIONS_TASK_LIST_TASK_TYPES: SelectOption[];
  SCH_TIP_DEVOPS_AUTHORITY_HANDOVER: string;
  SCH_LINK_RECYCLE_OPRATION: boolean;
  SCH_BUTTON_DEVOPS_TASK_DETAIL: boolean;
  SCH_TIP_DEVOPS_TASK_LEVEL: string;
  SCH_TIP_DEVOPS_DEVELOPER: string;
  SCH_TIP_DEVOPS_DEVOPSER: string;
  SCH_OPTIONS_DEVOPS_FLOW_NODE_TYPES: SelectOption[];
  SCH_BUTTON_SUB_SLA_TASK: boolean;
  SCH_TIP_TASK_DETAIL_RELAY: string;
  SCH_TYPE_JOB: boolean;
  SCH_OPTIONS_JOB_LIST_TASK_TYPES: SelectOption[];
  SCH_COL_TASK_LOG_DIAGNOSIS: boolean;
  SCH_BUTTON_JOB_DETAIL: boolean;
  SCH_OPTIONS_JOB_DETAIL_TASK_TYPES: SelectOption[];
  SCH_TIP_JOB_DETAIL_STATE: string;
  SCH_TIP_DEVOPS_MODIFY_APP: string;
  SCH_BUTTON_ENABLE_CREATE_TASK_PERSON_MODE: boolean;
  SCH_BUTTON_ENABLE_TASK_LEVEL_EDIT: boolean;
  SCH_BUTTON_CREAT_TABLE: boolean;
  SCH_INPUT_ALARM_RECEIVE_SFIM_GROUP: boolean; // 任务报警配置的接收群
  SCH_BUTTON_ALARM_CHECK_OPS_VIEW: boolean;
  GLOBAL_BUTTON_TIMEZONE_ENABLE: boolean;
  GLOBAL_TEXT_TIMEZONE_TENANT: string;
  GLOBAL_OPTIONS_TIMEZONE_SELECTOR: SelectOption[];
  SCH_BUTTON_ZIP_PUBLISH_FORCE_APPROVE: boolean;
}>;

export type PortalConfigs = Partial<{
  PORTAL: boolean;
  PORTAL_INDEX: boolean;
  PORTAL_INDEX_BANNER: boolean;
  PORTAL_INDEX_GUIDE: boolean;
  PORTAL_INDEX_MANUAL: boolean;
  PORTAL_INDEX_FAQ: boolean;
  PORTAL_INDEX_ALLPRODUCTS: boolean;
  PORTAL_INDEX_PRODUCT_NAVIGATION: boolean;
  PORTAL_INDEX_FEEDBACK: boolean;
  PORTAL_USER: boolean;
  PORTAL_USER_USERCENTER: boolean;
  PORTAL_USER_USERCENTER_HANDOVER: boolean;
  PORTAL_USER_CONSOLE: boolean;
  PORTAL_USER_PROCESS: boolean;
  PORTAL_USER_APPLICATION: boolean;
  PORTAL_APPLICATION: boolean;
  PORTAL_APPLICATION_TYPEINFO: boolean;
  PORTAL_PROCESS: boolean;
  PORTAL_PROCESS_PROXYSETTING: boolean;
  PROTAL_RESOURCE: boolean;
  PORTAL_BILL: boolean;
  PORTAL_VIEWPOINT: boolean;
  PORTAL_HELPCENTER: boolean;
  PORTAL_INDEX_LINK: string;
  PORTAL_INDEX_PERFORMANCE: boolean;
  PROTAL_SENSORSTRACK: boolean;
  PORTAL_APPLICATION_LOCK: boolean;
  PORTAL_OPENSFCHAT: boolean;
  PORTAL_OPS: boolean;
  PORTAL_DRCS: boolean;
  PROTAL_BUTTON_DATA_ANALYSIS_USED: string;
  PORTAL_ADMIN: boolean;
  PORTAL_ADMIN_USER: boolean;
  PORTAL_ADMIN_ROLE: boolean;
  PORTAL_ADMIN_AUTH: boolean;
  PORTAL_ADMIN_DISPATCH: boolean;
  PORTAL_ADMIN_WHITELIST: boolean;
  PORTAL_ADMIN_DEVICE: boolean;
  PORTAL_ADMIN_VNODES: boolean;
  PORTAL_ADMIN_NOTIFY: boolean;
  PORTAL_ADMIN_PROCESSTEMP: boolean;
  PORTAL_ADMIN_IDECONFIG: boolean;
  PORTAL_ADMIN_SEALCONFIG: boolean;
  PORTAL_ADMIN_UIOCLIMIT: boolean;
  PORTAL_ADMIN_AFFECTEDTASK: boolean;
  PORTAL_ADMIN_BUDGETCONFIG: boolean;
  PORTAL_ADMIN_TENANT: boolean;
  PORTAL_ADMIN_TENANTLEAGUE: boolean;
  PORTAL_ADMIN_APPLIST: boolean;
  PORTAL_ADMIN_DEMAND: boolean;
  PORTAL_ADMIN_MODEL: boolean;
  PORTAL_ADMIN_CONFIG: boolean;
  PORTAL_ADMIN_SLA: boolean;
  GLOBAL_BUTTON_TIMEZONE: boolean; // 时差是否开启
  GLOBAL_BUTTON_TIMEZONE_ENABLE: boolean;
  GLOBAL_TEXT_TIMEZONE_TENANT: string;
}>;

export type BdpConfigs = BdpSchConfigs & PortalConfigs;

export type CurrentUser = {
  extUserWarnWays?: any;
  auths: string[];
  prosList: number[];
  managerId: string;
  type: number;
  managerName: string;
  depName?: any;
  picUrl: string;
  epiboly: number;
  modifyTime: string;
  isOpen: boolean;
  tenantName: string;
  phone?: any;
  createTime: string;
  name: string;
  tenantId: string;
  depId?: any;
  id: string;
  isDisabled: number;
  department: string;
  email: string;
};

export type AllProject = {
  id: number;
  name: string;
  description: string;
  createTime: string;
  accountId: number;
  tenantId: number;
  isTech: number;
  inUse: number;
  sysCode: string;
  sysName: string;
  userDept: string;
  userOffice: string;
  userDeptId: string;
  userOfficeId: string;
  rdCenter: string;
  opMainTeam: string;
  businessLine: string;
  sysLevel: string;
  category: number;
  techProject?: any;
  inProcess: number;
};

export type Auth = {
  authKey: string;
  roleId: number;
  authName: string;
};

export type OwnerProject = {
  id: number;
  name: string;
  accountId: number;
  isLeader: boolean;
  auths: Auth[];
};

export type MyJurisdiction = {
  id: number;
  name: string;
  sysCode: string;
  roles: string;
  tenantId: number;
  enName: string;
};

export type AlarmAlertMode = {
  label: string;
  value: string;
};

export type MyProject = {
  id: number;
  name: string;
  description: string;
  createTime: string;
  accountId: number;
  tenantId: number;
  isTech: number;
  inUse: number;
  sysCode: string;
  sysName: string;
  userDept: string;
  userOffice: string;
  userDeptId: string;
  userOfficeId: string;
  rdCenter: string;
  opMainTeam: string;
  businessLine: string;
  sysLevel: string;
  category: number;
  techProject?: any;
  inProcess: number;
};

export type UserModelState = Partial<{
  currentUser: CurrentUser; // 当前用户信息
  allProjects: AllProject[];
  ownerProjects: OwnerProject[];
  myJurisdiction: MyJurisdiction[];
  isSealed: boolean;
  isProtectNet: boolean;
  unreadCount: number;
  // alarmAlertModes: AlarmAlertMode[];
  myProjects: MyProject[]; // 用户有权限的应用列表
  vNodeList: any[];
  projectMemebers: any[]; // 当前应用下的用户列表
  globalReady: boolean; // 项目初始数据是否准备好
  userStatusConfig: any;
  authorization: 0 | 1 | 2; // 当前应用是否已授权认证 0 未认证 1 已认证 2 切换中
  mode: ModeEnum; // 当前授权模式
  bdpConfigs: BdpConfigs;
  portalConfigEntry: PortalConfigs;
  shareCurrentTenantAppList: SelectOption[]; // 我有权限的当前租户下的所有应用
  myRootTenantAppList: SelectOption[]; // 我有权限的一级租户下的所有应用
  shareRootTenantAppList: SelectOption[]; // 我有权限的一级租户下的所有应用以及共享的租户下的应用
  rpsmId: number; // 全局选中的需求ID
  supportFlowNodeTypes: keyValue;
  supportTaskTypes: keyValue;
  privateConfig: keyValue;
}>;

export enum PRIVATE_CONFIG_KEYS {
  CONTENT_RTC_DEVOPS_FIELDS = 'CONTENT_RTC_DEVOPS_FIELDS',
  CONTENT_RTC_DEVOPS_OWNER = 'CONTENT_RTC_DEVOPS_OWNER',
  CONTENT_RTC_DEVOPS_ST_POS = 'CONTENT_RTC_DEVOPS_ST_POS',
  CONTENT_SCH_JM_BUS_TIME = 'CONTENT_SCH_JM_BUS_TIME',
  CONTENT_SCH_JM_FIELDS = 'CONTENT_SCH_JM_FIELDS',
  CONTENT_SCH_JM_OWNER = 'CONTENT_SCH_JM_OWNER',
  CONTENT_SCH_TM_FIELDS = 'CONTENT_SCH_TM_FIELDS',
  CONTENT_SCH_TM_OWNER = 'CONTENT_SCH_TM_OWNER',
  CONTENT_TEMP_ENGINE = 'CONTENT_TEMP_ENGINE',
  LAYOUT_MM_RTC_DET_TAB = 'LAYOUT_MM_RTC_DET_TAB',
  LAYOUT_MM_RTC_LEFT_NAVI = 'LAYOUT_MM_RTC_LEFT_NAVI',
  LAYOUT_MM_SCH_LEFT_NAVI = 'LAYOUT_MM_SCH_LEFT_NAVI',
  LAYOUT_MOD_FOLD_RTC = 'LAYOUT_MOD_FOLD_RTC',
  LAYOUT_MOD_FOLD_SCH = 'LAYOUT_MOD_FOLD_SCH',
  LAYOUT_MOD_FOLD_TEMP = 'LAYOUT_MOD_FOLD_TEMP',
  LAYOUT_MM_SCH_LEFT_NAVI_INDEX_TABTYPE = 'LAYOUT_MM_SCH_LEFT_NAVI_INDEX_TABTYPE', // 自定义配置中的第一个tab的tabKey
}

export type PrivateConfigItem = {
  key: string;
  value: string | null;
  label: string;
};

export interface PrivateConfig {
  [PRIVATE_CONFIG_KEYS.CONTENT_RTC_DEVOPS_FIELDS]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.CONTENT_RTC_DEVOPS_OWNER]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.CONTENT_RTC_DEVOPS_ST_POS]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.CONTENT_SCH_JM_BUS_TIME]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.CONTENT_SCH_JM_FIELDS]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.CONTENT_SCH_JM_OWNER]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.CONTENT_SCH_TM_FIELDS]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.CONTENT_SCH_TM_OWNER]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.CONTENT_TEMP_ENGINE]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.LAYOUT_MM_RTC_DET_TAB]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.LAYOUT_MM_RTC_LEFT_NAVI]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.LAYOUT_MM_SCH_LEFT_NAVI]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.LAYOUT_MOD_FOLD_RTC]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.LAYOUT_MOD_FOLD_SCH]: PrivateConfigItem[];
  [PRIVATE_CONFIG_KEYS.LAYOUT_MOD_FOLD_TEMP]: PrivateConfigItem[];
}
