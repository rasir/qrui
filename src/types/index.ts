import type { JSXChildren } from "@builder.io/qwik";





export interface SelectOption {
    label: string | JSXChildren;
    value: string | number;
    [key: string]: any;
  }

export interface keyValue {
    [key: string]: any;
  }