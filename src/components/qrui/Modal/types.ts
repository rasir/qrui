import type { CSSProperties, QRL } from "@builder.io/qwik";
import type { JSX } from "@builder.io/qwik/jsx-runtime";

export interface ModalProps {
    /* 是否显示 Modal */
    visible?: boolean;
    /* 标题 也可以通过slot传入 */
    title?: string;
    /* 宽度 */
    width?: string | number;
    /* 是否全屏 */
    fullscreen?: boolean;
    /* 是否需要遮罩层，默认 true false时遮罩层透明 */
    mask?: boolean;
    /* 遮罩层类名 */
    maskClass?: string;
    /* 自身是否插入至 body 元素上。 嵌套的 modal 必须指定该属性并赋值为 true */
    appendToBody?: boolean;
    /* 指定 Modal 挂载的 HTML 节点，false 为挂载在当前 dom */
    getContainer?: QRL<() => HTMLElement>;
    /* 容器类名 */
    className?: string;
    /* 点击遮罩是否可以关闭弹窗，默认是 */
    maskClosable?: boolean;
    /* 是否支持键盘 esc 关闭 */
    keyboard?: boolean;
    /* 可用于设置浮层的样式，调整浮层位置等 */
    style?: CSSProperties;
    /* 设置 Modal 的 z-index 默认1000 */
    zIndex?: number;
    /* 浮窗是否居中 */
    center?: boolean;
    /* 点击取消按钮 */
    onCancel?: QRL<() => void>;
    /* 点击确认按钮 */
    onOk?: QRL<() => void>;
    /* 是否显示底部按钮 */
    footer?: boolean;
    /* 自定义底部信息 */
    customFooter?: QRL<() => JSX.Element>;
    /* 是否可拖动 */
    draggable?: boolean;
}