import type { QRL } from "@builder.io/qwik";
import type { IMenuItem } from "../MenuItem/types";

export interface IMenu {
  duration?: number;
  activeKey?: string;
  mode?: "vertical" | "horizontal"; //菜单展示模式
  className?: string;
  collapse?: boolean; //是否水平折叠收起菜单（仅在 mode 为 vertical 时可用）
  width?: number; //菜单宽度（仅在 mode 为 horizontal 时可用）
  collapseWidth?: number; //水平折叠收起的宽度（仅在 mode 为 horizontal 时可用）
  theme?: "light" | "dark"; //菜单主题
  onMenuHanlder?: QRL<(item: IMenuItem) => void>;
}

export interface IMenuState {
  duration?: number;
  activeKey?: string;
  onHanlder: QRL<(item: IMenuItem) => void>;
}