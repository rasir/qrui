
export interface SpinProps {
    /* 是否为加载中状态 */
    spinning?: boolean;
    /* 当作为包裹元素时，可以自定义描述文案 */
    // tip?: string;
    /* 延迟显示加载效果的时间（防止闪烁） */
    // delay?: number;
    /* 加载指示符 */
    // indicator?: string;
    /* 组件大小，可选值为 */
    size?: 'small' | 'default' | 'large';
    /* 包装器的类属性 */
    wrapperClassName?: string;
}