import type { PropFunction } from "@builder.io/qwik";

export interface IMenuItem {
    className?: string;
    label: string;
    tabKey: string;
    value?: string;
    url?: string;
    active?: boolean;
    target?: string;
    custom?: boolean;
    onClick?: PropFunction<(item: IMenuItem) => void>;
    // render?: () => JSX.Element;
    menus?: IMenuItem[];
    [key: string]: any;
}