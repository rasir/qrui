import { $, Slot, component$, useContext, useStyles$ } from "@builder.io/qwik";
import type { IMenuItem } from "./types";
import styles from './index.css?inline'
import { menuContextId } from "../Menu";
import type { IMenuState } from "../Menu/types";

const MenuItem = component$<IMenuItem>((props) => {
    useStyles$(styles)

    const context = useContext<IMenuState>(menuContextId);

    const onClick$ = $(() => {
        props.onClick$ && props.onClick$(props)
        context.onHanlder(props);
    })

    return <li class={[props.className, 'qrui-menu-item flex m-[1px]']} onClick$={onClick$} >
        <div class={['qrui-menu-item-content whitespace-nowrap flex-1 flex items-center transition-all  cursor-pointer text-white hover:bg-[#4d4d4d] rounded-[4px]', {
            'bg-qrui-primary-base-color': props.tabKey && context.activeKey === props.tabKey,
        }]} style={{
            transitionDuration: `${context.duration || 300}ms`,
        }}>
            {!props.custom && (<span class="qrui-menu-item-content-icon"> <Slot name="icon" /></span>)}
            {props.custom && (<Slot />)}
            {!props.custom && (<span class="qrui-menu-item-content-label"> <Slot name="label" /></span>)}
        </div>
    </li>
})

export default MenuItem;