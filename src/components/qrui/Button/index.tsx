import { Slot, component$, useStyles$ } from "@builder.io/qwik";
import type { ButtonProps } from "./types";
import lineLoading from '../svgs/lineLoading.svg?raw'
import styles from './index.css?inline';

const Button = component$<ButtonProps>((props) => {
    useStyles$(styles);
    const {
        classNames,
        shape = "default",
        block,
        loading,
        type = 'default',
        onClick,
        size = 'middle',
        style,
        target,
        href,
        ghost,
        disabled,
        danger,
    } = props;
    return (
        <button
            onClick$={onClick}
            class={[
                "qrui-button",
                classNames,
                `qrui-button-size-${size}`,
                `qrui-button-type-${type}`,
                `qrui-button-shape-${shape}`,
                {
                    "qrui-button-block": block,
                    "qrui-button-ghost": ghost,
                    "qrui-button-danger": danger,
                }
            ]}
            style={style}
            disabled={disabled}
        >
            {
                href && !disabled ? (
                    <a href={href} target={target || "_self"} rel="noreferrer">
                        {loading && <span class="qrui-button-loading" dangerouslySetInnerHTML={lineLoading}></span>}
                        <Slot />
                    </a>
                ) : (
                    <span>
                        {loading && <span class="qrui-button-loading" dangerouslySetInnerHTML={lineLoading}></span>}
                        <Slot />
                    </span>
                )
            }
        </button>
    )
})

export default Button;