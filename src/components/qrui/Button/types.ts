import type { CSSProperties, QRL } from "@builder.io/qwik";

export interface ButtonProps {
    block?: boolean; // 将按钮宽度调整为其父宽度的选项
    classNames?: string; // 语义化结构 class
    danger?: boolean; // 设置危险按钮
    disabled?: boolean; // 设置按钮失效状态
    ghost?: boolean; // 幽灵属性，使按钮背景透明
    href?: string; // 点击跳转的地址，指定此属性 button 的行为和 a 链接一致
    // htmlType?: string; // 设置 button 原生的 type 值，可选值请参考 HTML 标准
    loading?: boolean ; // 设置按钮载入状态
    shape?: 'default' | 'circle' | 'round'; // 设置按钮形状
    size?: 'large' | 'middle' | 'small'; // 设置按钮大小
    style?: CSSProperties; // 语义化结构 style
    target?: string; // 相当于 a 链接的 target 属性，href 存在时生效
    type?: 'primary' | 'dashed' | 'link' | 'text' | 'default'; // 设置按钮类型,按钮有五种类型：主按钮、次按钮、虚线按钮、文本按钮和链接按钮。主按钮在同一个操作区域最多出现一次
    onClick?: QRL<(event: MouseEvent) => void>; // 点击按钮时的回调
}
