import type { CSSProperties } from "@builder.io/qwik";

export interface IIcon {
    type: string;
    color?: string;
    className?: string;
    style?: CSSProperties;
    size?: number;
    [key: string]: any;
  }