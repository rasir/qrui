import { component$ } from "@builder.io/qwik";
import type { IIcon } from "./types";

const Icon = component$<IIcon>(({
  type,
  color = '#fff',
  style,
  className = '',
  size = 20,
  ...restProps
}) => {
  return (
    <span class={className}>
      <svg
        // class={`bdpicon bdpicon-${size} ${className}`}
        class={['qrui-icon fill-current w-[1em] h-[1em]']}
        aria-hidden="true"
        {...restProps}
        style={{ color, fontSize: size, ...style }}
      >
        <use xlink:href={`#${type}`} />
      </svg>
    </span>
  );
});

export default Icon;
