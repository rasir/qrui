import type { CSSProperties } from "@builder.io/qwik";

export interface DividerProps {
    className?: string; // 分割线样式类
    dashed?: boolean; // 是否虚线
    orientation?: 'left' | 'right' | 'center'; // 分割线标题的位置 只支持 type = 'horizontal'
    orientationMargin?: string | number; // 标题和最近 left/right 边框之间的距离，去除了分割线，同时 orientation 必须为 left 或 right。如果传入 string 类型的数字且不带单位，默认单位是 px
    plain?: boolean; // 文字是否显示为普通正文样式
    style?: CSSProperties; // 分割线样式对象
    type?: 'horizontal' | 'vertical'; // 水平还是垂直类型
    size?: number;// 分割线宽度（horizontal）或者高度（vertical）
    margin?: number;// 间距
    lineWidth?: number; // 线宽，默认1
    color?: string; // 线颜色
  }
  