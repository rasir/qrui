import { Slot, component$ } from "@builder.io/qwik";
import type { DividerProps } from "./types";

const Divider = component$<DividerProps>((props) => {

    const {
        className,
        dashed,
        orientation,
        orientationMargin = 18,
        plain,
        style,
        type = 'horizontal',
        size,
        margin = 0,
        lineWidth = 1,
        color
    } = props;
    return <div class={[
        "qrui-divider relative",
        `qrui-divider-${type}`,
        {
            "border-dashed": dashed,
            "block": type === 'horizontal',
            "inline-block": type === 'vertical',
        },
        className,
    ]} style={{
        height: type === 'horizontal' ? margin * 2 || 48 : size || '100%',
        width: type === 'vertical' ? margin * 2 || 48 : size || '100%',
        ...(style || {})
    }}>
        <div class={[
            "qrui-divider-line absolute  border-gray-300",
            {
                "border-solid": !dashed,
                "border-dashed": dashed,
                "h-[1px] w-full top-[50%]": type === 'horizontal',
                "w-[1px] h-full left-[50%]": type === 'vertical',
            }
        ]}
            style={{
                borderTopWidth: type === 'horizontal' ? lineWidth : undefined,
                borderLeftWidth: type === 'vertical' ? lineWidth : undefined,
                borderColor: color
            }}
        ></div>
        {type === 'horizontal' && orientation &&
            <span class='qrui-divider-inner absolute bg-white px-6' style={{
                left: orientation === 'left' ? orientationMargin : orientation === 'center' ? "50%" : undefined,
                right: orientation === 'right' ? orientationMargin : undefined,
                top: '50%',
                transform: orientation === 'center' ? 'translate(-50%,-50%)' : 'translateY(-50%)',
                fontSize: plain ? undefined : 16,
                fontWeight: plain ? undefined : 500
            }}><Slot /></span>
        }
    </div>
})

export default Divider;