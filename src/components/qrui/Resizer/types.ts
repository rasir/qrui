import type { CSSProperties, QRL } from "@builder.io/qwik";

export interface ResizeProps {
    mode?: ResizeMode;
    onResizeStart?: QRL<(e: MouseEvent) => void>;
    onResize?: QRL<(e: MouseEvent, size: { width?: number, height?: number }) => void>;
    onResizeEnd?: QRL<(e: MouseEvent) => void>;
    maxWidth?: number;
    minWidth?: number;
    maxHeight?: number;
    minHeight?: number;
    onMaxWidth?: QRL<(e: MouseEvent,width: number) => void>;
    onMinWidth?: QRL<(e: MouseEvent,width: number) => void>;
    onMaxHeight?: QRL<(e: MouseEvent,height: number) => void>;
    onMinHeight?: QRL<(e: MouseEvent,height: number) => void>;
    className?: string;
    style?: CSSProperties;
}

export enum ResizeMode {
    Top = 'top',
    Right = 'right',
    Bottom = 'bottom',
    Left = 'left',
    LeftTop = 'left-top',
    RightTop = 'right-top',
    LeftBottom = 'left-bottom',
    RightBottom = 'right-bottom',
}