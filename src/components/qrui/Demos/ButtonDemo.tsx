import { $, component$ } from "@builder.io/qwik";
import Button from "../Button";


const ButtonDemo = component$(() => {
    const onClick = $(() => {
        console.log(18, '测试按钮')
    })

    return <div class="flex-1 overflow-auto p-10">
        <div>
            <div>尺寸和形状</div>
            <div>
                <Button shape="default" size="small" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button shape="round" size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button shape="circle" size="large" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点</Button>
            </div>
            <div>
                <Button disabled shape="default" size="small" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button disabled shape="round" size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button disabled shape="circle" size="large" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点</Button>
            </div>
        </div>
        <div>
            <div>按钮有五种类型：主按钮、次按钮、虚线按钮、文本按钮和链接按钮。主按钮在同一个操作区域最多出现一次</div>
            <div>
                <Button size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button size="middle" classNames="ml-10 mt-10" onClick={onClick}>点击</Button>
                <Button size="middle" classNames="ml-10 mt-10" type="dashed" onClick={onClick}>点击</Button>
                <Button size="middle" classNames="ml-10 mt-10" type="text" onClick={onClick}>点击</Button>
                <Button size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
            </div>
            <div>
                <Button disabled size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button disabled size="middle" classNames="ml-10 mt-10" onClick={onClick}>点击</Button>
                <Button disabled size="middle" classNames="ml-10 mt-10" type="dashed" onClick={onClick}>点击</Button>
                <Button disabled size="middle" classNames="ml-10 mt-10" type="text" onClick={onClick}>点击</Button>
                <Button disabled size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
            </div>
        </div>
        <div>
            <div>链接 link</div>
            <div>
                <Button size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
                <Button href="https://www.baidu.com" size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
                <Button target="_blank" href="https://www.baidu.com" size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
            </div>
            <div>
                <Button disabled size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
                <Button disabled href="https://www.baidu.com" size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
                <Button disabled target="_blank" href="https://www.baidu.com" size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
            </div>
        </div>
        <div>
            <div>danger 按钮</div>
            <div>
                <Button danger size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button danger size="middle" classNames="ml-10 mt-10" onClick={onClick}>点击</Button>
                <Button danger size="middle" classNames="ml-10 mt-10" type="dashed" onClick={onClick}>点击</Button>
                <Button danger size="middle" classNames="ml-10 mt-10" type="text" onClick={onClick}>点击</Button>
                <Button danger size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
            </div>
            <div>
                <Button disabled danger size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button disabled danger size="middle" classNames="ml-10 mt-10" onClick={onClick}>点击</Button>
                <Button disabled danger size="middle" classNames="ml-10 mt-10" type="dashed" onClick={onClick}>点击</Button>
                <Button disabled danger size="middle" classNames="ml-10 mt-10" type="text" onClick={onClick}>点击</Button>
                <Button disabled danger size="middle" classNames="ml-10 mt-10" type="link" onClick={onClick}>点击</Button>
            </div>
        </div>
        <div style={{ background: '#bec8c8' }}>
            <div>ghost 按钮</div>
            <div>
                <Button ghost size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button ghost size="middle" classNames="ml-10 mt-10" onClick={onClick}>点击</Button>
                <Button ghost size="middle" classNames="ml-10 mt-10" type="dashed" onClick={onClick}>点击</Button>
                <Button ghost danger size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
            </div>
            <div>
                <Button disabled ghost size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
                <Button disabled ghost size="middle" classNames="ml-10 mt-10" onClick={onClick}>点击</Button>
                <Button disabled ghost size="middle" classNames="ml-10 mt-10" type="dashed" onClick={onClick}>点击</Button>
                <Button disabled ghost danger size="middle" classNames="ml-10 mt-10" type="primary" onClick={onClick}>点击</Button>
            </div>
        </div>
    </div>
})

export default ButtonDemo;