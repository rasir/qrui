import { $, component$ } from "@builder.io/qwik";
import Button from "../Button";
import Space from "../Space";


const SpaceDemo = component$(() => {
    const onClick = $(() => {
        console.log(18, '测试按钮')
    })

    return <div class="flex-1 overflow-auto p-10">
        <Space block size={40} direction="vertical">
            <Space block size="large">
                align:start
                <Space align="start">
                    <Button shape="default" size="small" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="round" size="middle" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="circle" size="large" type="primary" onClick={onClick}>点</Button>
                </Space>
                <Space direction="vertical" align="start">
                    <Button shape="default" size="small" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="round" size="middle" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="circle" size="large" type="primary" onClick={onClick}>点</Button>
                </Space>
            </Space>
            <Space block size="large">
                align:center
                <Space align="center">
                    <Button shape="default" size="small" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="round" size="middle" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="circle" size="large" type="primary" onClick={onClick}>点</Button>
                </Space>
                <Space direction="vertical" align="center">
                    <Button shape="default" size="small" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="round" size="middle" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="circle" size="large" type="primary" onClick={onClick}>点</Button>
                </Space>
            </Space>
            <Space block size="large">
                align:end
                <Space align="end">
                    <Button shape="default" size="small" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="round" size="middle" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="circle" size="large" type="primary" onClick={onClick}>点</Button>
                </Space>
                <Space direction="vertical" align="end">
                    <Button shape="default" size="small" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="round" size="middle" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="circle" size="large" type="primary" onClick={onClick}>点</Button>
                </Space>
            </Space>
            <Space block size="large">
                align:baseline
                <Space align="baseline">
                    <Button shape="default" size="small" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="round" size="middle" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="circle" size="large" type="primary" onClick={onClick}>点</Button>
                </Space>
                <Space direction="vertical" align="baseline">
                    <Button shape="default" size="small" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="round" size="middle" type="primary" onClick={onClick}>点击</Button>
                    <Button shape="circle" size="large" type="primary" onClick={onClick}>点</Button>
                </Space>
            </Space>
        </Space>
    </div>
})

export default SpaceDemo;