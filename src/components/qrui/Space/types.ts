import type { CSSProperties } from "@builder.io/qwik";

type Size = 'small' | 'middle' | 'large' | number;
export interface SpaceProps {
    align?: 'start' | 'end' | 'center' | 'baseline'; // 对齐方式
    classNames?:string; // 语义化 className
    direction?: 'vertical' | 'horizontal'; // 间距方向
    size?: Size; // 间距大小
    style?: CSSProperties; // 语义化 style
    wrap?: boolean; // 是否自动换行
    block?: boolean;// 将宽度调整为父元素宽度的选项 
}
  