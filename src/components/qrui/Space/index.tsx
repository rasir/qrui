import { Slot, component$, useStyles$ } from "@builder.io/qwik";
import type { SpaceProps } from "./types";
import styles from './index.css?inline';


const Space = component$<SpaceProps>((props) => {
    useStyles$(styles)

    const {
        size = 'small',
        style = {},
        direction = 'horizontal',
        classNames,
        align,
        block,
        wrap,
    } = props;

    return <div class={[
        "qrui-space flex",
        `qrui-space-size-${size}`,
        {
            "flex-wrap": wrap,
            "inline-flex": !block,
            "flex": block,
            "flex-row": direction === 'horizontal',
            "flex-col": direction === 'vertical',
            "items-center": align === 'center',
            "items-start": align === 'start',
            "items-end": align === 'end',
            "items-baseline": align === 'baseline',
        },
        classNames,
    ]} style={{ ...style, gap: typeof size === 'number' ? size : undefined }}>
        <Slot />
    </div>
})

export default Space;