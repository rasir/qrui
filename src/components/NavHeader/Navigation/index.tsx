import { $, component$, useContext } from '@builder.io/qwik'
import { GlobalModalContext } from '~/models/global';
import type { IMenuItem } from '../../qrui/MenuItem/types';
import type { IMenu } from '../../qrui/Menu/types';
import { Menu, MenuItem } from '../../qrui';


export const Navigation = component$<IMenu>(() => {
    const globalState = useContext(GlobalModalContext);
    const { menus, activeNavKey } = globalState;

    const onMenuHanlder = $((item: IMenuItem) => {
        globalState.activeNavKey = item.tabKey;
    })


    return (
        <nav class="qrui-navigation px-[10px] h-[40px] w-full flex flex-row bg-slate-950">
            <Menu activeKey={activeNavKey} mode='horizontal' onMenuHanlder={onMenuHanlder}>
                {menus.map((menu) => (
                    <MenuItem key={menu.tabKey} tabKey={menu.tabKey}>
                        <span q:slot='label'>{menu.label}</span>
                    </MenuItem>
                ))}
            </Menu>
        </nav>
    )
})