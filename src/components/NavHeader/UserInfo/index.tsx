import { component$, useContext } from "@builder.io/qwik";
import { GlobalModalContext } from "~/models/global";


const UserInfo = component$(() => {
    const context = useContext(GlobalModalContext);

    return (
        <div class="flex items-center justify-center px-[10px]">
            <div class="whitespace-nowrap">{context.curentUser?.name}</div>
        </div>
    )
})

export default UserInfo;