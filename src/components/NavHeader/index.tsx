import { component$ } from "@builder.io/qwik"
import { Navigation } from "~/components/NavHeader/Navigation";
import logSvg from '~/assets/images/logo.svg'
import { Link } from "@builder.io/qwik-city";
import UserInfo from "./UserInfo";

const BdpHeader = component$(() => {
    return (
        <header class="w-full flex text-white items-center bg-slate-950 px-[10px]">
            <Link href="/" class="logos text-[14px] whitespace-nowrap flex justify-between items-center px-4 py-2">
                <img src={logSvg} alt="" width={24} height={24} />
                <div class="text-white font-bold ml-[10px]">丰数平台</div>
            </Link>
            <Navigation className="flex-1" />
            <UserInfo />
        </header>
    )
})

export default BdpHeader