import { $, component$, useSignal, useVisibleTask$ } from '@builder.io/qwik';
import type { LeftMenuProps } from './type';
import { useState } from './useState';
import type { IMenuItem } from '../qrui/MenuItem/types';
import { Icon, Menu, MenuItem } from '../qrui';
import Resizer from '../qrui/Resizer';
import { ResizeMode } from '../qrui/Resizer/types';


const LeftMenu = component$<LeftMenuProps>(() => {

    const [store, setStore] = useState();

    const { activeMenuKey, menus, collapse, dragable } = store;

    const width = useSignal<number>(160);

    const onMenuHanlder = $((item: IMenuItem) => {
        // 处理菜单点击事件
        setStore({ activeMenuKey: item.tabKey })
    })


    useVisibleTask$(({ track }) => {
        track(() => store.collapse);
        if (collapse) {
            width.value = 40;
        } else {
            width.value = 160;
        }
    })

    return (
        <div class="left-menu h-full bg-slate-950 flex flex-col transition-all duration-300 ease-linear relative" style={{
            width: `${width.value}px`
        }}>
            <Menu collapse={collapse} activeKey={activeMenuKey} mode="vertical" theme="dark" onMenuHanlder={onMenuHanlder}>
                <MenuItem custom active={activeMenuKey === 'home'} key="home" onClick$={$(() => setStore({ collapse: !collapse }))}>
                    <span class="flex flex-1 items-center justify-end"><Icon type="bdp-expand-right" className={[
                        "transition-all duration-300 ease-linear",
                        {
                            "rotate-180": collapse
                        }
                    ]} /></span>
                </MenuItem>
                {menus?.map((item) => (
                    <MenuItem
                        // active={activeMenuKey === item.key}
                        // {...item}
                        key={item.tabKey}
                        tabKey={item.tabKey}
                    // onClick$={$(() => onMenuHanlder(item))}
                    >
                        <Icon type={item.icon} q:slot='icon' />
                        <span q:slot='label' class="ml-[15px]">{item.label}</span>
                    </MenuItem>
                ))}
            </Menu>
            {dragable && !collapse && <Resizer mode={ResizeMode.Right} minWidth={160} />}
        </div>
    )
})

export default LeftMenu;