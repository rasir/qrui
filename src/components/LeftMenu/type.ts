import type { IMenuItem } from "../qrui/MenuItem/types";

export interface LeftMenuProps {
    
}

export interface LeftMenuState {
    menus?: (IMenuItem & {icon?:string})[];
    activeMenuKey?: string;
    collapse?: boolean;
    dragable?: boolean;
}
