import type { QRL } from "@builder.io/qwik";
import { $, useStore } from "@builder.io/qwik";
import type { LeftMenuState } from "./type";

export function useState(): [LeftMenuState, QRL<(value: Partial<LeftMenuState>) => void>] {
  const state = useStore<LeftMenuState>({
    collapse: false,
    dragable: true,
    menus: [
      {
        "tabKey": "daohang1",
        "label": "导航一",
        icon: "bdp-search"
      },
      {
        "tabKey": "daohang2",
        "label": "导航二",
        icon: 'bdp-coding'
      },
      {
        "tabKey": "daohang3",
        "label": "导航三",
        icon: "bdp-dir-full"
      },
      {
        "tabKey": "daohang4",
        "label": "导航四",
        icon: "bdp-paper"
      }
    ]
  });

  const setStore = $((value: LeftMenuState) => {
    Object.assign(state, value)
  })
  return [state, setStore]
}