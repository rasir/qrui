import { createContextId } from '@builder.io/qwik';
import { ModeEnum } from '~/types/enums';
import type { UserModelState } from '~/types/userModalTypes';
export const UserModalContext  = createContextId<UserModelState>('user-context');

export const userState:UserModelState = {
    currentUser: undefined,
    allProjects: [],
    ownerProjects: [],
    shareCurrentTenantAppList: [],
    myRootTenantAppList: [],
    myJurisdiction: [],
    isSealed: false,
    isProtectNet: false,
    unreadCount: 0,
    myProjects: [],
    vNodeList: [],
    projectMemebers: [],
    globalReady: false,
    userStatusConfig: {},
    authorization: 0,
    mode: ModeEnum.PERSON,
    bdpConfigs: {},
    supportFlowNodeTypes: {},
    supportTaskTypes: {},
    privateConfig: undefined,
  };
  