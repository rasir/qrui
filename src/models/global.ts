import { createContextId } from "@builder.io/qwik";
import i18next from "~/i18next";
import type { IMenuItem } from "~/components/qrui/MenuItem/types";
import type { CurrentUser } from "~/types/userModalTypes";
const { t } = i18next;

export interface GlobalModalState {
  activeNavKey?: string;
  menus: IMenuItem[];
  curentUser?: CurrentUser;
  globalLoading?: boolean;
}

export const GlobalModalContext =
  createContextId<GlobalModalState>("global-context");

export const globalModalState: GlobalModalState = {
  activeNavKey: "home",
  menus: [
    {
      label: t("首页"),
      tabKey: "home",
    },
    {
      label: t("组件"),
      tabKey: "comp",
    },
  ],
  curentUser: undefined,
  globalLoading: false,
};
