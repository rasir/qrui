import { component$, useContextProvider, useStore, useTask$, } from "@builder.io/qwik";
import {
  QwikCityProvider,
  RouterOutlet,
  ServiceWorkerRegister,
} from "@builder.io/qwik-city";
import { RouterHead } from "./components/router-head/router-head";

import "./global.css";
import type { UserModelState } from "./types/userModalTypes";
import { UserModalContext, userState } from "./models/user";
import type { GlobalModalState } from "./models/global";
import { GlobalModalContext, globalModalState } from "./models/global";

export default component$(() => {
  /**
   * The root of a QwikCity site always start with the <QwikCityProvider> component,
   * immediately followed by the document's <head> and <body>.
   *
   * Don't remove the `<head>` and `<body>` elements.
   */
  const globalStore = useStore<GlobalModalState>(globalModalState)
  useContextProvider(GlobalModalContext, globalStore)
  useTask$(() => {
    globalStore.activeNavKey = 'ide'
  })

  const userStore = useStore<UserModelState>(userState)
  useContextProvider(UserModalContext, userStore)


  return (
    <QwikCityProvider>
      <head>
        <meta charSet="utf-8" />
        <link rel="manifest" href="/manifest.json" />
        <RouterHead />
        <ServiceWorkerRegister />
        <script src="/iconfont.js"></script>
      </head>
      <body lang="en" class="h-[100vh] overflow-hidden">
        <RouterOutlet />
      </body>
    </QwikCityProvider>
  );
});
