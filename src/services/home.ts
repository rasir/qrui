import { fetchGet } from "~/utils/fetch";

export async function getUserServer(): Promise<any> {
    return await fetchGet("/api/user");
}