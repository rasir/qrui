import { component$, Slot, useContext, useStyles$, useVisibleTask$ } from "@builder.io/qwik";
import { routeLoader$ } from "@builder.io/qwik-city";
import type { RequestHandler } from "@builder.io/qwik-city";

// import Header from "../components/starter/header/header";
// import Footer from "../components/starter/footer/footer";

import styles from "./styles.css?inline";
import { GlobalModalContext } from "~/models/global";
import Spin from "~/components/qrui/Spin";

// export const useGetUser = routeLoader$(async () => {
//   const ret = await getUserServer();
//   return ret;
// })

export const onGet: RequestHandler = async ({ cacheControl }) => {
  // Control caching for this request for best performance and to reduce hosting costs:
  // https://qwik.dev/docs/caching/
  cacheControl({
    // Always serve a cached response by default, up to a week stale
    staleWhileRevalidate: 60 * 60 * 24 * 7,
    // Max once every 5 seconds, revalidate on the server to get a fresh version of this page
    maxAge: 5,
  });
};

export const useServerTimeLoader = routeLoader$(() => {
  return {
    date: new Date().toISOString(),
  };
});

export default component$(() => {
  useStyles$(styles);
  const globalModal = useContext(GlobalModalContext);

  return (
    <Spin wrapperClassName="h-full w-full" spinning={globalModal.globalLoading}>
      <main class="h-full">
        <Slot />
      </main>
    </Spin>
  );
});
