import { component$ } from "@builder.io/qwik";
import NavHeader from "~/components/NavHeader";
import LeftMenu from "~/components/LeftMenu";
import DividerDemo from "~/components/qrui/Demos/DividerDemo";
// import ButtonDemo from "~/components/qrui/Demos/ButtonDemo";
// import SpaceDemo from "~/components/qrui/Demos/SpaceDemo";


export default component$(() => {

    return (
        <div class="h-full flex flex-col">
            <NavHeader />
            <main class="flex-1 flex flex-col overflow-hidden">
                <div class="flex-1 flex flex-row overflow-hidden">
                    <LeftMenu />
                    <div class="flex-1 flex flex-col overflow-hidden">
                        {/* <ButtonDemo /> */}
                        {/* <SpaceDemo /> */}
                        <DividerDemo />
                    </div>
                </div>
            </main >
        </div >
    )
})