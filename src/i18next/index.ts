import type { Resource } from "i18next";
import i18next from "i18next";
import zhCN from "./zh/translation.json";
import enUS from "./en/translation.json";
import thTH from "./th/translation.json";

const resource: Resource = {
  zh: {
    translation: zhCN,
  },
  en: {
    translation: enUS,
  },
  th: {
    translation: thTH,
  },
};
const lng = "zh";
i18next.init({
  lng,
  resources: resource,
  interpolation: {
    escapeValue: false,
  },
  fallbackLng: "zh",
});

export default {
  ...i18next,
  t: (key: string, config: any = {}) =>
    i18next.t(key, config) as unknown as string,
};
